# Computer Vision

In this repository I show two examples of short projects that I did in the field of computer vision. These projects were undertaken as part of two UC Berkeley CS graduate courses:

1) [Introduction to Machine Learning](https://people.eecs.berkeley.edu/~jrs/189/).
2) [Computer Vision](https://inst.eecs.berkeley.edu/~cs280/sp18/).

The project from the Computer Vision course is based on classifying CIFAR-10 using CNNs, and the project from the Machine Learning course relies on a dataset homemade here at UC Berkeley by the students taking the course. In the latter dataset the students took photos of themselves in different scenarios while performing different exercises, such as squats, push-ups and so on. However the original images are not available here due to privacy concerns.