# Convolutional Neural Networks for CIFAR-10 classification

This project was part of a UC Berkeley graduate course on [Computer Vision](https://inst.eecs.berkeley.edu/~cs280/sp18/). 

Here, I developed several convolutional neural network models for classifying images from the CIFAR-10 dataset.